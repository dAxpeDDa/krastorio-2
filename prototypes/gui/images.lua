data:extend(
{
	-- About K2
	---
	{
		type     = "sprite",
		name     = "about-k2-preview",		
		filename = kr_graphic_mod_path .. "gui/previews/about-k2-preview.png",
		width    = 700,
		height   = 105,
		scale    = 0.70
	},
	-- New metals
	---
	{
		type     = "sprite",
		name     = "new-metals-preview",		
		filename = kr_graphic_mod_path .. "gui/previews/new-metals-preview.png",
		width    = 700,
		height   = 300,
		scale    = 0.70
	},
	-- Air purification
	---
	{
		type     = "sprite",
		name     = "air-purifier-preview-1",		
		filename = kr_technologies_icons_path .. "air-purifier.png",
		width    = 128,
		height   = 128,
		scale    = 1.5
	},
	{
		type     = "sprite",
		name     = "air-purifier-preview-2",
		filename = kr_recipes_icons_path .. "restore-used-pollution-filter.png",
		width    = 128,
		height   = 128,
		scale    = 1
	},
	---
	-- Creep
	---
	{
		type     = "sprite",
		name     = "kr-creep-preview-1",
		filename = kr_graphic_mod_path .. "gui/previews/creep-preview.png",
		width    = 700,
		height   = 518,
		scale    = 0.70
	},
	{
		type     = "sprite",
		name     = "kr-creep-preview-2",
		filename = kr_graphic_mod_path .. "gui/previews/bio-lab.png",
		width    = 232,
		height   = 244
	},
	---
	{
		type     = "sprite",
		name     = "kr-fuels-preview",
		filename = kr_graphic_mod_path .. "gui/previews/fuels-preview.png",
		width    = 274,
		height   = 117
	},
	-- New gun play
	---
	{
		type     = "sprite",
		name     = "kr-new-gun-play-preview",
		filename = kr_graphic_mod_path .. "gui/previews/new-gun-play-preview.png",
		width    = 700,
		height   = 327,
		scale    = 0.70
	},
	-- Fusion
	---
	{
		type     = "sprite",
		name     = "kr-fusion-reactor-preview",
		filename = kr_graphic_mod_path .. "gui/previews/fusion-reactor-preview.png",
		width    = 382,
		height   = 400
	},
	{
		type     = "sprite",
		name     = "kr-advanced-steam-turbine-preview",
		filename = kr_graphic_mod_path .. "gui/previews/advanced-steam-turbine-preview.png",
		width    = 227,
		height   = 133
	},
	---
	{
		type     = "sprite",
		name     = "kr-intergalactic-transceiver-preview",
		filename = kr_graphic_mod_path .. "gui/previews/intergalactic-transceiver-preview.png",
		width    = 700,
		height   = 485,
		scale    = 0.70
	},
	{
		type     = "sprite",
		name     = "kr-inserters-hotkey-preview",
		filename = kr_graphic_mod_path .. "gui/previews/inserters-hotkey-preview.png",
		width    = 700,
		height   = 244,
		scale    = 0.70
	},
	{
		type     = "sprite",
		name     = "kr-matter-preview",
		filename = kr_technologies_icons_path .. "backgrounds/matter.png",
		width    = 128,
		height   = 128
	},
	{
		type     = "sprite",
		name     = "roboports-hotkey-preview",
		filename = kr_graphic_mod_path .. "gui/previews/roboports-hotkey-preview.png",
		width    = 700,
		height   = 250,
		scale    = 0.70
	},
	{
		type     = "sprite",
		name     = "kr-shelter-preview",
		filename = kr_graphic_mod_path .. "gui/previews/shelter-preview.png",
		width    = 584,
		height   = 379,
		scale    = 0.80
	},
	{
		type     = "sprite",
		name     = "kr-crusher-preview",
		filename = kr_graphic_mod_path .. "gui/previews/crusher-preview.png",
		width    = 232,
		height   = 236
	}	
})